﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleUnitTestApplication2.Extensions
{
    public static class ObjectExtensions
    {

        public static JsonResult ToJson(this object obj)
        {
            JsonSerializerSettings jsonSettings = new JsonSerializerSettings()
            {
                Formatting = Formatting.Indented
            };

            return new JsonResult(obj, jsonSettings);
        }
    }
}
