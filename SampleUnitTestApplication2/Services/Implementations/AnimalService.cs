﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SampleUnitTestApplication2.Data;
using SampleUnitTestApplication2.Data.Models;

namespace SampleUnitTestApplication2.Services.Implementations
{
    public class AnimalService : IAnimalService
    {
        private ApplicationDbContext _db;
        public AnimalService(ApplicationDbContext db)
        {
            _db = db;
        }

        public IEnumerable<Animal> GetAnimals()
        {
            return _db.Animals.ToList();
        }
    }
}
