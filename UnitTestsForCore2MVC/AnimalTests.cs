﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using Moq;
using SampleUnitTestApplication2.Services.Implementations;
using SampleUnitTestApplication2.Data.Models;
using SampleUnitTestApplication2.Controllers;
using SampleUnitTestApplication2.Services;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace UnitTestsForCore2MVC
{
    [TestFixture]
    public class AnimalTests
    {
        [Test]
        public void AnimalsController_ShouldReturnProperAnimals()
        {
            //Arrange
            var animalService = new Mock<IAnimalService>();

            IEnumerable<Animal> animals = new List<Animal>
            {
               new Animal { Name = "Cornflakes", Type = "Tiger" },
               new Animal { Name = "Maik", Type = "Cat" }
            };

            animalService.Setup(x => x.GetAnimals()).Returns(animals);

            var animalController = new AnimalsController(animalService.Object);

            //act
            var returnValue = (JsonResult) animalController.Get();

            var realValue = returnValue.Value;

            //var asd = JsonConvert.DeserializeObject<IEnumerable<Animal>>(realValue);
            
            Assert.AreEqual(1, 1);
        }
    }
}
