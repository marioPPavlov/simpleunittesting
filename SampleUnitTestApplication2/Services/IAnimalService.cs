﻿using SampleUnitTestApplication2.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleUnitTestApplication2.Services
{
    public interface IAnimalService
    {
        IEnumerable<Animal> GetAnimals();
    }
}
